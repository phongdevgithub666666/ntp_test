package treatment;

public class MyBigNumber {
	public String sum(String stn1, String stn2) {
		String temp = "";
		if (stn1.length() < stn2.length()) {
			temp = stn1;
			stn1 = stn2;
			stn2 = temp;
		}
		String sum = "";
		int asyncStep = stn1.length() - stn2.length();
		String pow = "";
		int soNho = 0;
		for (int i = 0; i < stn1.length(); i++) {
			if (stn1.length() - 1 - i - asyncStep < 0) {
				System.out.println("Bước" + (i + 1) + " Ta cộng 2 số " + stn1.charAt(stn1.length() - 1 - i) + " và cộng với số nhớ "
						+ "0" + " ra kết quả "
						+ Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) % 10 + " nhớ " + soNho);
				String subSum = String.valueOf(
						((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))) % 10 + soNho) == 10 ? 0
								: ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))) % 10 + soNho))
						.concat(pow);
				sum = subSum.substring(0, subSum.length() - i).concat(sum);
				soNho = ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) + soNho) >= 10) ? 1 : 0;
			} else {
				System.out.println("Bước" + (i + 1) + " Ta cộng 2 số " + stn1.charAt(stn1.length() - 1 - i) + " và cộng với số nhớ "
						+ stn2.charAt(stn1.length() - 1 - i - asyncStep) + " ra kết quả "
						+ (Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
								+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))) % 10
						+ " nhớ " + soNho);
				String subSum = String.valueOf(((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
						+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))) % 10
						+ soNho) == 10
								? 0
								: ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) + Integer
										.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))) % 10
										+ soNho))
						.concat(pow);
				sum = subSum.substring(0, subSum.length() - i).concat(sum);
				soNho = ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
						+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))
						+ soNho) >= 10) ? 1 : 0;

			}
			pow = pow.concat("0");
			if (i == (stn1.length() - 1)) {
				if (soNho == 1) {
					sum = "1".concat(sum);
				}
			}
		}
		return sum;
	}

//	public String sum1(String stn1, String stn2) {
//		String temp = "";
//		String sum = "";
//		int asyncStep = stn1.length() - stn2.length();
//		String pow = "";
//		int soNho = 0;
//
//		if (stn1.length() < stn2.length()) {
//			for (int i = 0; i < stn2.length(); i++) {
//				if (stn1.length() - 1 - i - asyncStep < 0) {
//					System.out.println("Bước" + (i + 1) + " Ta cộng 2 số " + stn1.charAt(stn1.length() - 1 - i) + " và "
//							+ "0" + " ra kết quả "
//							+ Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) % 10 + " nhớ "
//							+ soNho);
//
//					String subSum = String.valueOf(
//							((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))) % 10 + soNho) == 10
//									? 0
//									: ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))) % 10
//											+ soNho))
//							.concat(pow);
//					sum = subSum.substring(0, subSum.length() - i).concat(sum);
//					soNho = ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) + soNho) >= 10) ? 1
//							: 0;
//
//				} else {
//					System.out.println("Bước" + (i + 1) + " Ta cộng 2 số " + stn1.charAt(stn1.length() - 1 - i) + " và "
//							+ stn2.charAt(stn1.length() - 1 - i - asyncStep) + " ra kết quả "
//							+ (Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
//									+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep))))
//									% 10
//							+ " nhớ " + soNho);
//
//					String subSum = String.valueOf(((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
//							+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))) % 10
//							+ soNho) == 10
//									? 0
//									: ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) + Integer
//											.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep))))
//											% 10 + soNho))
//							.concat(pow);
//					sum = subSum.substring(0, subSum.length() - i).concat(sum);
//					soNho = ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
//							+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))
//							+ soNho) >= 10) ? 1 : 0;
//
//				}
//
//				pow = pow.concat("0");
//				if (i == (stn1.length() - 1)) {
//					if (soNho == 1) {
//						sum = "1".concat(sum);
//					}
//
//				}
//
//			}
//		} else {
//			for (int i = 0; i < stn1.length(); i++) {
//				if (stn1.length() - 1 - i - asyncStep < 0) {
//					System.out.println("Bước" + (i + 1) + " Ta cộng 2 số " + stn1.charAt(stn1.length() - 1 - i) + " và "
//							+ "0" + " ra kết quả "
//							+ Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) % 10 + " nhớ "
//							+ soNho);
//
//					String subSum = String.valueOf(
//							((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))) % 10 + soNho) == 10
//									? 0
//									: ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))) % 10
//											+ soNho))
//							.concat(pow);
//					sum = subSum.substring(0, subSum.length() - i).concat(sum);
//					soNho = ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) + soNho) >= 10) ? 1
//							: 0;
//
//				} else {
//					System.out.println("Bước" + (i + 1) + " Ta cộng 2 số " + stn1.charAt(stn1.length() - 1 - i) + " và "
//							+ stn2.charAt(stn1.length() - 1 - i - asyncStep) + " ra kết quả "
//							+ (Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
//									+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep))))
//									% 10
//							+ " nhớ " + soNho);
//
//					String subSum = String.valueOf(((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
//							+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))) % 10
//							+ soNho) == 10
//									? 0
//									: ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i))) + Integer
//											.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep))))
//											% 10 + soNho))
//							.concat(pow);
//					sum = subSum.substring(0, subSum.length() - i).concat(sum);
//					soNho = ((Integer.valueOf(String.valueOf(stn1.charAt(stn1.length() - 1 - i)))
//							+ Integer.valueOf(String.valueOf(stn2.charAt(stn1.length() - 1 - i - asyncStep)))
//							+ soNho) >= 10) ? 1 : 0;
//
//				}
//
//				pow = pow.concat("0");
//				if (i == (stn1.length() - 1)) {
//					if (soNho == 1) {
//						sum = "1".concat(sum);
//					}
//
//				}
//
//			}
//		}
//		return sum;
//	}
}
